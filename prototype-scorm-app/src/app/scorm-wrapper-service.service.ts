import { APP_ID, Injectable } from '@angular/core';

interface SCORM12API {
  LMSInitialize(dummy: string): string;
  LMSGetValue(key: string): string;
}
declare global {
  interface Window {
    API?: {
      setCallback(api: SCORM12API): void;
    }
  }
}

interface RuntimeData {
  student_name: string;
}

class ServerSideAPI implements SCORM12API {
  constructor(readonly initialData: RuntimeData) {

  }
  LMSInitialize(dummy: string): string {
    return "";
  }
  LMSGetValue(cmiKey: string): string {
    switch (cmiKey) {
      case 'cmi.core.student_name':
        return this.initialData.student_name;
      default:
        return '';
    }
  }
}

@Injectable({
  providedIn: 'root'
})
export class ScormWrapperServiceService {

  constructor() { }

  setup(runtimeData: RuntimeData) {
    const wrapper = new ServerSideAPI(runtimeData);
    window.API?.setCallback(wrapper);
  }
}
