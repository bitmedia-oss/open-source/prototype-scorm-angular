import { TestBed } from '@angular/core/testing';

import { ScormWrapperServiceService } from './scorm-wrapper-service.service';

describe('ScormWrapperServiceService', () => {
  let service: ScormWrapperServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ScormWrapperServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
