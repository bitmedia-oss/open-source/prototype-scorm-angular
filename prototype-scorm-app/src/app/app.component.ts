import { Component } from '@angular/core';
import { ScormWrapperServiceService } from './scorm-wrapper-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'prototype-scorm-app';
  constructor(readonly scormWrapper: ScormWrapperServiceService) {

  }
  startContent() {
    this.scormWrapper.setup({
      student_name: 'Max Mustermann'
    })
    window.open('/assets/sco.html', 'content', 'width=300,height=300,menubar=no,toolbar=no,location=no')
  }
}
